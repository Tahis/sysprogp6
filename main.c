#include "mask.h"
#include <stdio.h>

static int mask_owner, mask_group, mask_other;
char in;

int main() {
	printf("Práctica 6 de Programación de Sistemas\n\nprograma pregunta al usuario permisos para owner, group y other\nOwner:");
	printf("\n1. Permiso lectura para owner? [s/n] ");
	scanf("%c",&in);
	getchar();
	switch(in)
	{
		case 's': 
			setPermiso(&mask_owner,'r',SET);
			break;
		case 'n':
			setPermiso(&mask_owner,'r',UNSET);
			break;
	}
	printf("\n2. Permiso escritura para owner? [s/n] ");
	scanf("%c",&in);
	getchar();
	switch(in)
	{
		case 's': 
			setPermiso(&mask_owner,'w',SET);
			break;
		case 'n':
			setPermiso(&mask_owner,'w',UNSET);
			break;
	}
	printf("\n3. Permiso ejecución para owner? [s/n] ");
	scanf("%c",&in);
	getchar();
	switch(in)
	{
		case 's': 
			setPermiso(&mask_owner,'x',SET);
			break;
		case 'n':
			setPermiso(&mask_owner,'x',UNSET);
			break;
	}
	printf("\nGroup: \n4. Permiso lectura para group? [s/n] ");
	scanf("%c",&in);
	getchar();
	switch(in)
	{
		case 's': 
			setPermiso(&mask_group,'r',SET);
			break;
		case 'n':
			setPermiso(&mask_group,'r',UNSET);
			break;
	}
	printf("\n5. Permiso escritura para group? [s/n] ");
	scanf("%c",&in);
	getchar();
	switch(in)
	{
		case 's': 
			setPermiso(&mask_group,'w',SET);
			break;
		case 'n':
			setPermiso(&mask_group,'w',UNSET);
			break;
	}
	printf("\n6. Permiso ejecución para group? [s/n] ");
	scanf("%c",&in);
	getchar();
	switch(in)
	{
		case 's': 
			setPermiso(&mask_group,'x',SET);
			break;
		case 'n':
			setPermiso(&mask_group,'x',UNSET);
			break;
	}
	printf("\nOther: \n7. Permiso lectura para other? [s/n] ");
	scanf("%c",&in);
	getchar();
	switch(in)
	{
		case 's': 
			setPermiso(&mask_other,'r',SET);
			break;
		case 'n':
			setPermiso(&mask_other,'r',UNSET);
			break;
	}
	printf("\n8. Permiso escritura para other? [s/n] ");
	scanf("%c",&in);
	getchar();
	switch(in)
	{
		case 's': 
			setPermiso(&mask_other,'w',SET);
			break;
		case 'n':
			setPermiso(&mask_other,'w',UNSET);
			break;
	}
	printf("\n9. Permiso ejecución para other? [s/n] ");
	scanf("%c",&in);
	getchar();
	switch(in)
	{
		case 's': 
			setPermiso(&mask_other,'x',SET);
			break;
		case 'n':
			setPermiso(&mask_other,'x',UNSET);
			break;
	}
	printf("\n\nMáscara octal: %d%d%d\n",mask_owner,mask_group,mask_other);
}
